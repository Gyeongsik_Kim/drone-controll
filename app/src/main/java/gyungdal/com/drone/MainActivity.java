package gyungdal.com.drone;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    private JoyStick leftJoyStick;
    private JoyStick rightJoyStick;
    private static final String LEFT_JOYSTICK = "LEFT JOY STICK";
    private static final String RIGHT_JOYSTICK = "RIGHT JOY STICK";
    private static final String TAG = MainActivity.class.getName();
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        leftJoyStick = (JoyStick)findViewById(R.id.left);
        leftJoyStick.setOnJoystickMoveListener(new JoyStick.OnJoystickMoveListener() {
            @Override
            public void onValueChanged(int angle, int power, int direction) {
                Log.i(LEFT_JOYSTICK, "angle : " + angle);
                Log.i(LEFT_JOYSTICK, "power : " + power);
                switch(direction){
                    case JoyStick.FRONT :
                        Log.i(LEFT_JOYSTICK, "Direction : " + "FRONT");
                        break;
                    case JoyStick.FRONT_RIGHT :
                        Log.i(LEFT_JOYSTICK, "Direction : " + "FRONT RIGHT");
                        break;
                    case JoyStick.RIGHT :
                        Log.i(LEFT_JOYSTICK, "Direction : " + "RIGHT");
                        break;
                    case JoyStick.RIGHT_BOTTOM :
                        Log.i(LEFT_JOYSTICK, "Direction : " + "RIGHT BOTTOM");
                        break;
                    case JoyStick.BOTTOM :
                        Log.i(LEFT_JOYSTICK, "Direction : " + "BOTTOM");
                        break;
                    case JoyStick.BOTTOM_LEFT :
                        Log.i(LEFT_JOYSTICK, "Direction : " + "BOTTOM LEFT");
                        break;
                    case JoyStick.LEFT :
                        Log.i(LEFT_JOYSTICK, "Direction : " + "LEFT");
                        break;
                    case JoyStick.LEFT_FRONT :
                        Log.i(LEFT_JOYSTICK, "Direction : " + "LEFT FRONT");
                        break;
                    default:
                        Log.i(LEFT_JOYSTICK, "Direction : " + "CENTER");
                }
            }
        }, JoyStick.DEFAULT_LOOP_INTERVAL);

        rightJoyStick = (JoyStick)findViewById(R.id.right);
        rightJoyStick.setOnJoystickMoveListener(new JoyStick.OnJoystickMoveListener() {
            @Override
            public void onValueChanged(int angle, int power, int direction) {
                Log.i(RIGHT_JOYSTICK, "angle : " + angle);
                Log.i(RIGHT_JOYSTICK, "power : " + power);
                switch(direction){
                    case JoyStick.FRONT :
                        Log.i(RIGHT_JOYSTICK, "Direction : " + "FRONT");
                        break;
                    case JoyStick.FRONT_RIGHT :
                        Log.i(RIGHT_JOYSTICK, "Direction : " + "FRONT RIGHT");
                        break;
                    case JoyStick.RIGHT :
                        Log.i(RIGHT_JOYSTICK, "Direction : " + "RIGHT");
                        break;
                    case JoyStick.RIGHT_BOTTOM :
                        Log.i(RIGHT_JOYSTICK, "Direction : " + "RIGHT BOTTOM");
                        break;
                    case JoyStick.BOTTOM :
                        Log.i(RIGHT_JOYSTICK, "Direction : " + "BOTTOM");
                        break;
                    case JoyStick.BOTTOM_LEFT :
                        Log.i(RIGHT_JOYSTICK, "Direction : " + "BOTTOM LEFT");
                        break;
                    case JoyStick.LEFT :
                        Log.i(RIGHT_JOYSTICK, "Direction : " + "LEFT");
                        break;
                    case JoyStick.LEFT_FRONT :
                        Log.i(RIGHT_JOYSTICK, "Direction : " + "LEFT FRONT");
                        break;
                    default:
                        Log.i(RIGHT_JOYSTICK, "Direction : " + "CENTER");
                }
            }
        }, JoyStick.DEFAULT_LOOP_INTERVAL);
    }
}